FROM openjdk:8-jdk-alpine

ARG JAR_FILE=target/*.jar
ARG JAR_PROPERTY_FILE=Robot_11.properties

WORKDIR /usr/local/runme

COPY ${JAR_FILE} app.jar
COPY ${JAR_PROPERTY_FILE} .

ENTRYPOINT ["java","-jar","app.jar"]
