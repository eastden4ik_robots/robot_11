package ru.sviridoff.bots.telebot.handlers;

import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import ru.sviridoff.bots.telebot.TeleBot.CovidBot;
import ru.sviridoff.bots.telebot.exception.HttpRequestError;
import ru.sviridoff.bots.telebot.models.Countries;
import ru.sviridoff.bots.telebot.models.Country;
import ru.sviridoff.bots.telebot.models.World;
import ru.sviridoff.framework.logger.Logger;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.toIntExact;

public class BaseHandler {

    private static final Logger logger = new Logger("@COVID_19_" + CovidBot.class.getName() + "@");
    private static final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss dd.MM.yy");

    public static SendMessage start(Update update) {
        logger.info("Forming start reply for chat_id: " + update.getMessage().getChatId());
        return new SendMessage()
                .setChatId(update.getMessage().getChatId())
                .setText("Бот показывает сводную информацию по зараженным короновируом лицам, в разных странах.\n" +
                        "Для просмотра списка доступных команд введите /help.");
    }
    public static SendMessage world(Update update, String apiWorld) {
        logger.info("Forming World_Info reply for chat_id: " + update.getMessage().getChatId());
        HttpResponse<World> world = Unirest.get(apiWorld)
                .asObject(World.class);

        if (world.isSuccess()) {
            String msg = "Сводная информация по миру.\n" +
                    "Информация актуальна на " + sdf.format(world.getBody().getMUpdated()) + "\n" +
                    "1- Подтверждено: " + world.getBody().getMCases() + " (+" + world.getBody().getMTodayCases() + ")\n" +
                    "2- Погибло: " + world.getBody().getMDeaths() + " (+" + world.getBody().getMTodayDeaths() + ")\n" +
                    "3- Выздоровело: " + world.getBody().getMRecovered() + " (+" + world.getBody().getMTodayRecovered() + ")\n" +
                    "4- Протестировано: " + world.getBody().getMTests() + "\n" +
                    "5- Общая численность: " + world.getBody().getMPopulation();
            return new SendMessage()
                    .setChatId(update.getMessage().getChatId())
                    .setText(msg);
        } else
            throw new HttpRequestError(world.getStatus() + ", " + world.getStatusText());
    }
    public static SendMessage countries(Update update) {
        logger.info("Forming countries keyboard reply for chat_id: " + update.getMessage().getChatId());
        SendMessage message = new SendMessage()
                .setChatId(update.getMessage().getChatId())
                .setText("Доступные страны: \n");
        InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();

        int rows = Countries.list().size() % 3 == 0 ? Countries.list().size() / 3 : Countries.list().size() / 3 + 1;
        int mod = Countries.list().size() % 3;

        int k = 0;
        for (int i = 0; i < rows; i++) {
            List<InlineKeyboardButton> row = new ArrayList<>();
            for (int j = 0; j < 3; j++) {
                if ((i == rows - 1) && (mod != 0)) {
                    if (j <= mod - 1) {
                        row.add(new InlineKeyboardButton().setText(Countries.list().get(k)).setCallbackData(Countries.list().get(k)));
                        k ++;
                    }
                } else {
                    row.add(new InlineKeyboardButton().setText(Countries.list().get(k)).setCallbackData(Countries.list().get(k)));
                    k ++;
                }
            }
            rowsInline.add(row);
        }
        markupInline.setKeyboard(rowsInline);
        message.setReplyMarkup(markupInline);
        return message;
    }
    public static EditMessageText country(Update update, String apiCountry, String country) {
        logger.setDebugLevel(true);
        logger.debug(String.format(apiCountry, country));
        logger.info("Forming Country_Info reply for chat_id: " + update.getCallbackQuery().getId());
        HttpResponse<Country> response = Unirest.get(String.format(apiCountry, country))
                .asObject(Country.class);

        logger.debug(response.getStatus() + ", " + response.getStatusText());
        logger.debug(response.getBody().toString());
        if (response.isSuccess()) {

            Country c = response.getBody();
            String msg = "Сводная информация по миру.\n" +
                    "Информация по стране " + c.getMCountryInfo().getMFlag() + " " + country + " актуальна на " + sdf.format(c.getMUpdated()) + "\n" +
                    "1- Подтверждено: " + c.getMCases() + " (+" + c.getMTodayCases() + ")\n" +
                    "2- Погибло: " + c.getMDeaths() + " (+" + c.getMTodayDeaths() + ")\n" +
                    "3- Выздоровело: " + c.getMRecovered() + " (+" + c.getMTodayRecovered() + ")\n" +
                    "4- Протестировано: " + c.getMTests() + "\n" +
                    "5- Общая численность: " + c.getMPopulation();
            return new EditMessageText()
                    .setChatId(update.getCallbackQuery().getMessage().getChatId())
                    .setMessageId(toIntExact(update.getCallbackQuery().getMessage().getMessageId()))
                    .setText(msg);
        } else
            throw new HttpRequestError(response.getStatus() + ", " + response.getStatusText());
    }
    public static SendMessage help(Update update) {
        return new SendMessage()
                .setChatId(update.getMessage().getChatId())
                .setText("Бот для мониторинга вируса COVID-19 имеет всего 2 команды: \n" +
                        "1. /countries - мониторинг конкретной страны.\n" +
                        "2. /all - мониторинг всего мира.\n");
    }

}
