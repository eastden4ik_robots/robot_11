
package ru.sviridoff.bots.telebot.models;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Country extends Base {

    @SerializedName("country")
    private String mCountry;
    @SerializedName("continent")
    private String mContinent;
    @SerializedName("countryInfo")
    private CountryInfo mCountryInfo;

}
