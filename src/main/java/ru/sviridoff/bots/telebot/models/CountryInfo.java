
package ru.sviridoff.bots.telebot.models;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class CountryInfo {

    @SerializedName("flag")
    private String mFlag;
    @SerializedName("iso2")
    private String mIso2;
    @SerializedName("iso3")
    private String mIso3;
    @SerializedName("lat")
    private Long mLat;
    @SerializedName("long")
    private Double mLong;
    @SerializedName("_id")
    private Long m_id;

}
