
package ru.sviridoff.bots.telebot.models;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class World extends Base {

    @SerializedName("affectedCountries")
    private Long mAffectedCountries;

}
