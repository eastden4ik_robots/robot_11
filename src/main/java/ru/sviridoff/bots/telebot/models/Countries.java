package ru.sviridoff.bots.telebot.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Countries {

    public static List<String> list() {
        return new ArrayList<>(Arrays.asList(
                "Austria",
                "Belarus",
                "Belgium",
                "Bosnia",
                "Bulgaria",
                "Czechia",
                "Denmark",
                "Estonia",
                "Finland",
                "France",
                "Germany",
                "Greece",
                "Hungary",
                "Italy",
                "Latvia",
                "Netherlands",
                "Norway",
                "Poland",
                "Russia",
                "Serbia",
                "Slovakia",
                "Slovenia",
                "Spain",
                "Sweden",
                "Switzerland",
                "UK",
                "Ukraine", //////////////
                "Afghanistan",
                "Armenia",
                "Azerbaijan",
                "China",
                "Cyprus",
                "Georgia",
                "Hong Kong",
                "India",
                "Iran",
                "Israel",
                "Japan",
                "Kazakhstan",
                "Tajikistan",
                "Thailand",
                "Uzbekistan",
                "Vietnam", ////////////
                "Canada",
                "Mexico",
                "USA"));
    }


}
