
package ru.sviridoff.bots.telebot.models;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Base {

    @SerializedName("active")
    private Long mActive;
    @SerializedName("activePerOneMillion")
    private Double mActivePerOneMillion;
    @SerializedName("cases")
    private Long mCases;
    @SerializedName("casesPerOneMillion")
    private Long mCasesPerOneMillion;
    @SerializedName("critical")
    private Long mCritical;
    @SerializedName("criticalPerOneMillion")
    private Double mCriticalPerOneMillion;
    @SerializedName("deaths")
    private Long mDeaths;
    @SerializedName("deathsPerOneMillion")
    private Double mDeathsPerOneMillion;
    @SerializedName("oneCasePerPeople")
    private Long mOneCasePerPeople;
    @SerializedName("oneDeathPerPeople")
    private Long mOneDeathPerPeople;
    @SerializedName("oneTestPerPeople")
    private Long mOneTestPerPeople;
    @SerializedName("population")
    private Long mPopulation;
    @SerializedName("recovered")
    private Long mRecovered;
    @SerializedName("recoveredPerOneMillion")
    private Double mRecoveredPerOneMillion;
    @SerializedName("tests")
    private Long mTests;
    @SerializedName("testsPerOneMillion")
    private Double mTestsPerOneMillion;
    @SerializedName("todayCases")
    private Long mTodayCases;
    @SerializedName("todayDeaths")
    private Long mTodayDeaths;
    @SerializedName("todayRecovered")
    private Long mTodayRecovered;
    @SerializedName("updated")
    private Long mUpdated;

}
