package ru.sviridoff.bots.telebot.TeleBot;


import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import ru.sviridoff.bots.telebot.models.Countries;
import ru.sviridoff.bots.telebot.provider.ConfigProvider;
import ru.sviridoff.framework.logger.Logger;
import static ru.sviridoff.bots.telebot.handlers.BaseHandler.*;

public class CovidBot extends TelegramLongPollingBot {

    private final ConfigProvider provider;
    private static final Logger logger = new Logger("@COVID_19_" + CovidBot.class.getName() + "@");

    public CovidBot(ConfigProvider config) {
        provider = config;
    }

    public void onUpdateReceived(Update update) {
        logger.setDebugLevel(true);
        if (update.hasMessage() && update.getMessage().hasText()) {
            String message_text = update.getMessage().getText();
            long chat_id = update.getMessage().getChatId();
            logger.info("Бот получил от " + chat_id + ", сообщение " + message_text);

            switch (message_text) {
                case "/start": {
                    try {
                        execute(start(update));
                    } catch (TelegramApiException ex) {
                        logger.error(ex.getMessage());
                    }
                    break;
                }
                case "/countries" : {
                    try {
                        execute(countries(update));
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                    break;
                }
                case "/all": {
                    try {
                        execute(world(update, provider.getApiWorldUrl()));
                    } catch (TelegramApiException ex) {
                        logger.error(ex.getMessage());
                    }
                    break;
                }
                default: {
                    try {
                        execute(help(update));
                    } catch (TelegramApiException ex) {
                        logger.error(ex.getMessage());
                    }
                    break;
                }
            }

        } else if (update.hasCallbackQuery()) {
            String call_data = update.getCallbackQuery().getData();
            logger.debug(String.valueOf(Countries.list().stream().anyMatch(call_data::equalsIgnoreCase)));
            if (Countries.list().stream().anyMatch(call_data::equalsIgnoreCase)) {
                try {
                    execute(country(update, provider.getApiCountryUrl(), call_data));
                } catch (TelegramApiException e) {
                    logger.error(e.getMessage());
                }
            }
        }
    }

    public String getBotUsername() {
        return provider.getBotUsername();
    }

    public String getBotToken() {
        return provider.getBotToken();
    }
}



