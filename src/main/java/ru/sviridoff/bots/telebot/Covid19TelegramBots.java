package ru.sviridoff.bots.telebot;


import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import ru.sviridoff.bots.telebot.TeleBot.CovidBot;
import ru.sviridoff.bots.telebot.provider.ConfigProvider;
import ru.sviridoff.framework.logger.Logger;

public class Covid19TelegramBots {

    private static final Logger logger = new Logger("@COVID_19_" + Covid19TelegramBots.class.getName() + "@");

    public static void main(String[] args) {

        ConfigProvider config = new ConfigProvider()
                .setUp("./Robot_11.properties");

        ApiContextInitializer.init();
        TelegramBotsApi botsApi = new TelegramBotsApi();

        try {
            botsApi.registerBot(new CovidBot(config));
            logger.info("Telegram bot для проверки распространения вируса Covid_19.");
        } catch (TelegramApiException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }


    }

}
