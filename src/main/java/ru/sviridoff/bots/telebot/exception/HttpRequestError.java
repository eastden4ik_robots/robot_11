package ru.sviridoff.bots.telebot.exception;

public class HttpRequestError extends RuntimeException {

    public HttpRequestError(String message) {
        super(message);
    }

}
