package ru.sviridoff.bots.telebot.provider;



import lombok.Getter;
import ru.sviridoff.framework.props.Parameters;

public class ConfigProvider {

    @Getter
    private String botToken = null;
    @Getter
    private String botUsername = null;

    @Getter
    private String apiWorldUrl = null;
    @Getter
    private String apiCountryUrl = null;

    public ConfigProvider setUp(String properties) {

        Parameters parameters = new Parameters(properties);
        botToken = (String) parameters.getValue("TELEGRAM.TOKEN");
        botUsername = (String) parameters.getValue("TELEGRAM.USERNAME");

        apiWorldUrl = (String) parameters.getValue("API.WORLD");
        apiCountryUrl = (String) parameters.getValue("API.COUNTRY");
        return this;
    }
}